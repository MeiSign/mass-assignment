package app;

import java.io.IOException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonParseException;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void should_createComment_When_ValidRequest()
        throws JsonParseException, IOException {

        String jsonInString = "{\"content\":\"my comment\"}";
        ObjectMapper mapper = new ObjectMapper();
		Comment myComment = mapper.readValue(jsonInString, Comment.class);

        String commentAsString = mapper.writeValueAsString(myComment);

        assertThat(commentAsString, containsString("my comment"));
        assertThat(commentAsString, containsString("content"));
    }
}
